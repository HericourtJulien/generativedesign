var gulp = require('gulp');
// Tâche SASS
var sass = require('gulp-sass');
// Créer une tâche compile-styles
gulp.task('compile-styles', function () {
    return gulp.src('./styles/scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./styles/css/'));
});
// Tâche watch
gulp.task('watch', function () {
    gulp.watch('./styles/**/*.scss', ['compile-styles']);
});
/**
 * Tâches par défaut
 * Elles sont exécutées avec la commande «gulp»
 **/
gulp.task('default', ['watch'], function () {
});