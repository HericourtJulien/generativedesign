/***************************
 * Cross Browser AnimationFrame
 **************************/

window.requestAnimationFrame = (function () {
    return window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

window.AudioContext = window.AudioContext || window.webkitAudioContext;

/***************************
 * SoundAnalyser
 **************************/

var soundAnalyser = (function () {
    var SoundAnalyser = {
        el: null,
        context: new AudioContext(),
        sound: null,
        source: null,
        analyser: null,
        url: "./song.mp3",
        freqs: [],
        nbYPoints: null,
        nbXPoints: null,
        nbTotalPoints: null,
        sizeBaseYPoints: 100,
        sizeBaseXPoints: 100,
        sizeFinalXPoints: null,
        sizeFinalYPoints: null,
        bg: document.querySelector(".bg"),

        init: function () {
            this.nbXPoints = Math.floor(window.innerWidth / this.sizeBaseXPoints);
            this.nbYPoints = Math.floor((window.innerHeight - 50) / this.sizeBaseYPoints);

            this.sizeFinalXPoints = window.innerWidth / this.nbXPoints;
            this.sizeFinalYPoints = (window.innerHeight - 50) / this.nbYPoints;
            this.nbTotalPoints = this.nbXPoints * this.nbYPoints;

            this.el = document.querySelector("#my-sound");

            this.el.addEventListener("change", this.uploadSound.bind(this));
            this.animPres();
        },

        animPres: function () {
            var tl = new TimelineMax();
            var el = document.querySelector(".launch");
            el.querySelector("h1").innerHTML = el.querySelector("h1").innerHTML.replace(/(\w)/g, "<span>$&</span>");
            tl.staggerFrom(el.querySelectorAll("h1 span"), .6, {opacity: 0}, .1, "+=1");
            tl.from(el.querySelector("h2"), .6, {y: 100});
            tl.from(el.querySelector("p"), 1, {opacity: 0}, "+=1.2");
            tl.from(el.querySelector("#uploadSound"), 1, {opacity: 0}, "+=1.5");
        },

        uploadSound: function (e) {
            var reader = new FileReader();
            reader.addEventListener('load', (function () {
                this.url = reader.result;
                this.loadSound();
            }).bind(this));
            reader.readAsDataURL(e.target.files[0]);
        },

        loadSound: function () {
            var request = new XMLHttpRequest();
            request.open('GET', this.url, true);
            request.responseType = "arraybuffer";
            request.onload = (function () {
                this.context.decodeAudioData(request.response, this.soundLoaded.bind(this), function () {
                    console.log("Données audios erreur");
                });
            }).bind(this);
            request.send();
        },

        soundLoaded: function (buffer) {
            this.source = this.context.createBufferSource();
            this.source.buffer = buffer;

            this.analyser = this.context.createAnalyser();
            this.analyser.fftSize = 1024;
            this.analyser.smoothingTimeConstant = .9;

            this.analyser.connect(this.context.destination);

            this.freqs = new Uint8Array(this.analyser.frequencyBinCount);

            document.querySelector(".sound-name").innerHTML = this.el.value;
            var tl = new TimelineMax();
            tl.to(document.querySelector(".sound-name"), 1, {opacity: 1});
            tl.to(document.querySelector("#launch"), 2, {opacity: 1});

            document.querySelector("#launch").addEventListener('click', this.launchSound.bind(this));

            this.buildSep();
        },

        launchSound: function () {
            var tl = new TimelineMax();
            tl.to(document.querySelector(".launch"), 1, {autoAlpha: 0});
            tl.to(document.querySelector("footer"), 1, {y: 0});

            document.querySelector("footer").addEventListener("mouseover", this.openInput.bind(this));

            var answer = new Answer();
            answer.init();

            this.source.connect(this.analyser);
            this.source.start();
        },

        openInput: function () {
            var el = document.querySelector("footer");
            TweenMax.to(el, .3, {y: -60});
            el.addEventListener("mouseleave", this.closeInput.bind(this, el));
        },

        closeInput: function (el) {
            TweenMax.to(el, .3, {y: 0});
        },

        buildSep: function () {
            var idImg = Math.floor(Math.random() * 10);
            var i;
            var j;
            for (i = 0; i < this.nbYPoints; i++) {
                for (j = 0; j < this.nbXPoints; j++) {
                    var divImg = document.createElement("div");
                    divImg.classList.add('img');
                    divImg.setAttribute("style", "width: " + this.sizeFinalXPoints + "px; height: " + this.sizeFinalYPoints + "px");

                    var divViewport = document.createElement("div");
                    divViewport.classList.add('viewport');
                    divViewport.setAttribute("style", "width: " + this.sizeFinalXPoints + "px; height: " + this.sizeFinalYPoints + "px");

                    var img = document.createElement('img');
                    img.setAttribute("src", "./img/bg-" + idImg + ".jpg");
                    img.setAttribute("data-id", idImg);
                    img.setAttribute("style", "transform: translate3d(-" + (j * this.sizeFinalXPoints) + "px,-" + (i * this.sizeFinalYPoints) + "px,0)");

                    divViewport.appendChild(img)
                    divImg.appendChild(divViewport);
                    this.bg.appendChild(divImg);
                }
            }
            var imgWin = document.createElement('img');
            imgWin.setAttribute("src", "./img/bg-" + idImg + ".jpg");
            document.querySelector(".congrats").appendChild(imgWin);

            this.draw();
        },

        draw: function () {
            this.analyser.getByteFrequencyData(this.freqs);

            var freqs = this.freqs.slice(this.freqs.length / 2 - this.nbTotalPoints - 30, this.freqs.length / 2 - 30);
            // var freqs = this.freqs.slice(0, this.nbTotalPoints);

            var i = 0;
            for (i; i < this.nbTotalPoints; i++) {
                var current = document.querySelector(".img:nth-child(" + (i + 1) + ") .viewport");

                var size;
                freqs[i] / 2 < 100 ? size = freqs[i] / 2 : size = 100;

                current.setAttribute("style", "width: " + size + "px; height: " + size + "px;");
            }

            requestAnimationFrame(this.draw.bind(this));
        }
    };

    return SoundAnalyser;
})();

/***************************
 * Answer
 **************************/

var Answer = function () {
};

Answer.el = null;
Answer.id = null;
Answer.answer = null;
Answer.movies = null;
Answer.alerts = null;

Answer.prototype.init = function () {
    this.el = document.querySelector("footer form");
    this.id = document.querySelector(".img img").getAttribute("data-id");
    this.movies = [
        {"id": 0, "name": "Mission impossible"},
        {"id": 1, "name": "Jurassic World"},
        {"id": 2, "name": "Suicide Squad"},
        {"id": 3, "name": "Insaisissables"},
        {"id": 4, "name": "Warcraft"},
        {"id": 5, "name": "Captain America"},
        {"id": 6, "name": "Mise à l'épreuve"},
        {"id": 7, "name": "Batman"},
        {"id": 8, "name": "Hunger Games"},
        {"id": 9, "name": "Divergente"},
        {"id": 10, "name": "Deadpool"},
    ];
    this.build();
};

Answer.prototype.build = function () {
    this.answer = this.movies[this.id].name;
    this.alerts = document.querySelector(".alerts");
    this.el.querySelector("#btnAnswer").addEventListener('click', this.checkAnswer.bind(this));
};

Answer.prototype.checkAnswer = function (e) {
    e.preventDefault();
    var right;

    this.answer.toLowerCase() == this.el.querySelector("#textAnswer").value.toLowerCase() ? right = true : right = false;
    right ? this.alerts.innerHTML = "Bien joué !" : this.alerts.innerHTML = "Dommage... Essaie encore !";

    var tl = new TimelineMax();
    tl.to(this.alerts, .4, {opacity: 1});
    tl.to(this.alerts, .6, {scale: 2, ease: Bounce.easeOut}, "-=.4");
    tl.to(this.alerts, .4, {opacity: 0}, "+=.5");
    tl.to(this.alerts, .4, {scale: 0});
    tl.to(this.alerts, 0, {onComplete: function () {
        right ? winningPart() : "";
    }}, "+=1");

    this.el.querySelector("#textAnswer").value = "";
};

function winningPart() {
    var tl = new TimelineMax();
    tl.to(document.querySelector('.congrats'), 1.5, {autoAlpha: 1});
    tl.to(document, 0, {onComplete: function () {
        location.reload();
    }}, "+=5");
}

/***************************
 * App
 **************************/

var app = (function (soundAnalyser) {
    var App = {

        soundAnalyser: null,

        init: function () {
            this.soundAnalyser = soundAnalyser;
            document.addEventListener("DOMContentLoaded", this.loaded.bind(this));
        },

        loaded: function () {
            this.soundAnalyser.init();
        }

    };

    return App;
})(soundAnalyser);

/***************************
 * Launch
 **************************/

app.init();